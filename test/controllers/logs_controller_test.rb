require 'test_helper'

class LogsControllerTest < ActionController::TestCase
  setup do
    @meeting = meetings(:one)
    @log = logs(:one)
  end

  test "should get index" do
    get :index, params: { meeting_id: @meeting }
    assert_response :success
  end

  test "should get new" do
    get :new, params: { meeting_id: @meeting }
    assert_response :success
  end

  test "should create log" do
    assert_difference('Log.count') do
      post :create, params: { meeting_id: @meeting, log: @log.attributes }
    end

    assert_redirected_to meeting_log_path(@meeting, Log.last)
  end

  test "should show log" do
    get :show, params: { meeting_id: @meeting, id: @log }
    assert_response :success
  end

  test "should get edit" do
    get :edit, params: { meeting_id: @meeting, id: @log }
    assert_response :success
  end

  test "should update log" do
    put :update, params: { meeting_id: @meeting, id: @log, log: @log.attributes }
    assert_redirected_to meeting_log_path(@meeting, Log.last)
  end

  test "should destroy log" do
    assert_difference('Log.count', -1) do
      delete :destroy, params: { meeting_id: @meeting, id: @log }
    end

    assert_redirected_to meeting_logs_path(@meeting)
  end
end
