require 'test_helper'

class TasksControllerTest < ActionController::TestCase
  setup do
    @component = components(:one)
    @task = tasks(:one)
  end

  test "should get index" do
    get :index, params: { component_id: @component }
    assert_response :success
  end

  test "should get new" do
    get :new, params: { component_id: @component }
    assert_response :success
  end

  test "should create task" do
    assert_difference('Task.count') do
      post :create, params: { component_id: @component, task: @task.attributes }
    end

    assert_redirected_to component_task_path(@component, Task.last)
  end

  test "should show task" do
    get :show, params: { component_id: @component, id: @task }
    assert_response :success
  end

  test "should get edit" do
    get :edit, params: { component_id: @component, id: @task }
    assert_response :success
  end

  test "should update task" do
    put :update, params: { component_id: @component, id: @task, task: @task.attributes }
    assert_redirected_to component_task_path(@component, Task.last)
  end

  test "should destroy task" do
    assert_difference('Task.count', -1) do
      delete :destroy, params: { component_id: @component, id: @task }
    end

    assert_redirected_to component_tasks_path(@component)
  end
end
