// This file is automatically compiled by Webpack, along with any other files
// present in this directory. You're encouraged to place your actual application logic in
// a relevant structure within app/javascript and only use these pack files to reference
// that code so it'll be compiled.
import "chartkick/chart.js"
import Rails from "@rails/ujs"
import Turbolinks from "turbolinks"
import * as ActiveStorage from "@rails/activestorage"
import "channels"
import "chartkick/chart.js"
import "chartkick"

// import "bootstrap-sass/assets/javascripts/bootstrap/dropdown";
// import "bootstrap-sass/assets/javascripts/bootstrap/modal";
// import "bootstrap-sass/assets/javascripts/bootstrap/tooltip";
// import "bootstrap-sass/assets/javascripts/bootstrap/popover";
// import "../stylesheets/style";

require("chartkick")
require("chart.js")
Rails.start()
Turbolinks.start()
ActiveStorage.start()
