class Log < ApplicationRecord
  include PgSearch::Model
  
  multisearchable  against: [:title, :content],
    update_if: :title_changed?,
    update_if: :content_changed?
  


  belongs_to :meeting

  validates :severity, inclusion: {in: ['normal', 'critical']} 

  SEVERITY_OPTIONS = [['Normal','normal'],['Critic','critical']]
end
