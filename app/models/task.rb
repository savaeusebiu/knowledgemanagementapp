class Task < ApplicationRecord
  belongs_to :user_detail
  belongs_to :component
  accepts_nested_attributes_for :component, :user_detail

  validates :estimation, inclusion: {in: [0,1, 2, 3, 5, 8, 13, 21]} 
  validates :developer_feedback, inclusion: {in: ["underestimated", "supraestimated", "perfectly-estimated"]} 

  ESTIMATION_OPTIONS = [['0SP',0],
                      ['1SP',1],
                      ['2SP',2],
                      ['3SP',3],
                      ['5SP',5],
                      ['8SP',8],
                      ['13SP',13],
                      ['21SP',21],]
  
  DEVELOPER_FEEDBACK_OPTIONS = [["Subestimat","underestimated"],
                                ["Perfect estimat","perfectly-estimated"],
                                ["Supraestimat","supraestimated"]]


end
