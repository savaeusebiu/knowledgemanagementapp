class Meeting < ApplicationRecord
  has_many :logs, dependent: :delete_all
  has_many :appoiments, dependent: :delete_all
  has_many :users, through: :appoiments
end
