class User < ApplicationRecord
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable, :trackable and :omniauthable
  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :validatable

  has_many :appoiments
  has_many :meetings, through: :appoiments
  has_many :product_owner_teams, class_name: 'Teams', foreign_key: 'product_owner_id'
  has_many :scrum_master_teams, class_name: 'Teams', foreign_key: 'scrum_master_id'
  has_many :teams, through: :user_details
end
