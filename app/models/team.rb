class Team < ApplicationRecord
  belongs_to :product_owner, :class_name => 'User'
  belongs_to :scrum_master, :class_name => 'User'
  has_many :teams, through: :user_details
  has_many :components
end
