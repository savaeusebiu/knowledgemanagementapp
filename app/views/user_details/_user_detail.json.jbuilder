json.extract! user_detail, :id, :user_id, :team_id, :first_name, :last_name, :created_at, :updated_at
json.url user_detail_url(user_detail, format: :json)
