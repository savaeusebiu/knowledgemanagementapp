json.extract! component_task, :id, :title, :description, :first_commit, :last_commit, :estimation, :developer_feedback, :user_detail_id, :component_id, :created_at, :updated_at
json.url component_task_url(component_task, format: :json)
