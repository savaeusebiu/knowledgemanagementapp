json.extract! meeting_log, :id, :title, :content, :severity, :date, :meeting_id, :created_at, :updated_at
json.url meeting_log_url(meeting_log, format: :json)
