json.extract! team, :id, :product_owner_id, :scrum_master_id, :name, :created_at, :updated_at
json.url team_url(team, format: :json)
