json.extract! component, :id, :team_id, :name, :description, :created_at, :updated_at
json.url component_url(component, format: :json)
