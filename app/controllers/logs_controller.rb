class LogsController < ApplicationController
  before_action :set_meeting
  before_action :set_log, only: [:show, :edit, :update, :destroy]

  # GET meetings/1/logs
  def index
    @logs = @meeting.logs
  end

  # GET meetings/1/logs/1
  def show
  end

  # GET meetings/1/logs/new
  def new
    @log = @meeting.logs.build
  end

  # GET meetings/1/logs/1/edit
  def edit
  end

  # POST meetings/1/logs
  def create
    @log = @meeting.logs.build(log_params)

    if @log.save
      redirect_to(@log.meeting)
    else
      render action: 'new'
    end
  end

  # PUT meetings/1/logs/1
  def update
    if @log.update_attributes(log_params)
      redirect_to([@log.meeting, @log], notice: 'Log was successfully updated.')
    else
      render action: 'edit'
    end
  end

  # DELETE meetings/1/logs/1
  def destroy
    @log.destroy

    redirect_to meeting_logs_url(@meeting)
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_meeting
      @meeting = Meeting.find(params[:meeting_id])
    end

    def set_log
      @log = @meeting.logs.find(params[:id])
    end

    # Only allow a trusted parameter "white list" through.
    def log_params
      params.require(:log).permit(:title, :content, :severity, :date, :meeting_id)
    end
end
