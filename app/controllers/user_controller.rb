class UserController < ApplicationController
  before_action :authenticate_user!

  PgSearch.multisearch_options = {
    :using => [:tsearch, :trigram],
    :ignoring => :accents
    }

  def index
    @meetings = Meeting.joins(:appoiments).where(appoiments: {user_id: current_user.id})
    
    logs = []
    @meetings.each do |meeting|
      meeting.logs.each do |log|
        if "critical" == log[:severity] 
         logs.push([meeting.name, log])
        end
      end
    end
    @critical_logs = logs.sort_by {|log| log.second.date}.reverse

    ### Used for getting the User team id ###
    @id = UserDetail.find(current_user.id).team_id



    ### Search feature 
    @search_logs = []
    if !params[:search].nil?
      @meetings = Meeting.joins(:appoiments).where(appoiments: {user_id: current_user.id})
      search_logs = []
      Log.where("lower(title) LIKE ? OR lower(content) LIKE ?", 
        "%#{params[:search].downcase}%", 
        "%#{params[:search].downcase}%").each do |log|
          @meetings.each do |meeting|
            if meeting.id == log.meeting_id
              search_logs.push([meeting.name, log])
            end
          end
      end
      @search_logs = search_logs.sort_by {|log| log.second.date}.reverse
       ### clear search
      params[:search] == nil
    end
    
  end

end


