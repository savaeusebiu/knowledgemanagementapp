class TasksController < ApplicationController
  before_action :set_component
  before_action :set_task, only: [:show, :edit, :update, :destroy]

  # GET components/1/tasks
  def index
    @tasks = @component.tasks
  end

  # GET components/1/tasks/1
  def show
    # Feedback estimation 
    case @task.developer_feedback
      when "perfectly-estimated"
        @feedback_task = "Estimată corespunzător"
      when "supraestimated"
        @feedback_task = "Supraestimată"
      when "underestimated"
        @feedback_task = "Subestimată"
      else
        @feedback_task = "Nu există activități"
    end 
    
    # Team of the developer
    @team = Team.find(Component.find(@task.component_id).team_id)

    # Responsable developer 
    @developer = UserDetail.find(@task.user_detail_id)

  end

  # GET components/1/tasks/new
  def new
    @task = @component.tasks.build
  end

  # GET components/1/tasks/1/edit
  def edit
    @current_user_details = UserDetail.where(user_id: current_user.id).first
  end

  # POST components/1/tasks
  def create
    @task = @component.tasks.build(task_params)

    if @task.save
      redirect_to(@task.component)
    else
      render action: 'new'
    end
  end

  # PUT components/1/tasks/1
  def update
    if @task.update_attributes(task_params)
      redirect_to([@task.component, @task], notice: 'Task was successfully updated.')
    else
      render action: 'edit'
    end
  end

  # DELETE components/1/tasks/1
  def destroy
    @task.destroy

    redirect_to component_tasks_url(@component)
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_component
      @component = Component.find(params[:component_id])
    end

    def set_task
      @task = @component.tasks.find(params[:id])
    end

    # Only allow a trusted parameter "white list" through.
    def task_params
      params.require(:task).permit(:title, :description, :first_commit, :last_commit, :estimation, :developer_feedback, :user_detail_id, :component_id)
    end
end
