class AppoimentsController < ApplicationController
  before_action :set_appoiment, only: %i[ show edit update destroy ]

  # GET /appoiments or /appoiments.json
  def index
    @appoiments = Appoiment.all
  end

  # GET /appoiments/1 or /appoiments/1.json
  def show
  end

  # GET /appoiments/new
  def new
    @appoiment = Appoiment.new
  end

  # GET /appoiments/1/edit
  def edit
  end

  # POST /appoiments or /appoiments.json
  def create
    @appoiment = Appoiment.new(appoiment_params)

    respond_to do |format|
      if @appoiment.save
        format.html { redirect_to @appoiment, notice: "Appoiment was successfully created." }
        format.json { render :show, status: :created, location: @appoiment }
      else
        format.html { render :new, status: :unprocessable_entity }
        format.json { render json: @appoiment.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /appoiments/1 or /appoiments/1.json
  def update
    respond_to do |format|
      if @appoiment.update(appoiment_params)
        format.html { redirect_to @appoiment, notice: "Appoiment was successfully updated." }
        format.json { render :show, status: :ok, location: @appoiment }
      else
        format.html { render :edit, status: :unprocessable_entity }
        format.json { render json: @appoiment.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /appoiments/1 or /appoiments/1.json
  def destroy
    @appoiment.destroy
    respond_to do |format|
      format.html { redirect_to appoiments_url, notice: "Appoiment was successfully destroyed." }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_appoiment
      @appoiment = Appoiment.find(params[:id])
    end

    # Only allow a list of trusted parameters through.
    def appoiment_params
      params.require(:appoiment).permit(:user_id, :meeting_id)
    end
end
