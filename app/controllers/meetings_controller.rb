class MeetingsController < ApplicationController
  before_action :set_meeting, only: %i[ show edit update destroy ]

  # GET /meetings or /meetings.json
  def index
    @meetings = Meeting.all
  end

  # GET /meetings/1 or /meetings/1.json
  def show
    @log = @meeting.logs.build
    
    @meeting_logs = []
    @meeting.logs.each do |log|
      if log.persisted?
        @meeting_logs.push(log)
      end
    end
    @meeting_logs = @meeting_logs.sort_by {|log| log.date}.reverse
  end

  # GET /meetings/new
  def new
    @meeting = Meeting.new
  end

  # GET /meetings/1/edit
  def edit
  end

  # POST /meetings or /meetings.json
  def create
    @meeting = Meeting.new(meeting_params)

    respond_to do |format|
      if @meeting.save
        format.html { redirect_to @meeting, notice: "Meeting was successfully created." }
        format.json { render :show, status: :created, location: @meeting }
      else
        format.html { render :new, status: :unprocessable_entity }
        format.json { render json: @meeting.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /meetings/1 or /meetings/1.json
  def update
    respond_to do |format|
      if @meeting.update(meeting_params)
        format.html { redirect_to @meeting, notice: "Meeting was successfully updated." }
        format.json { render :show, status: :ok, location: @meeting }
      else
        format.html { render :edit, status: :unprocessable_entity }
        format.json { render json: @meeting.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /meetings/1 or /meetings/1.json
  def destroy
    @meeting.destroy
    respond_to do |format|
      format.html { redirect_to meetings_url, notice: "Meeting was successfully destroyed." }
      format.json { head :no_content }
    end
  end

  def toggle_subscribe
    user_subscribe = User.find(current_user.id)
    meeting_subscribe = Meeting.find(params[:id])
    Appoiment.create(meeting:meeting_subscribe, user: user_subscribe)
    redirect_to meetings_url
  end

  def toggle_unsubscribe
    user_subscribe = User.find(current_user.id)
    meeting_subscribe = Meeting.find(params[:id])
    Appoiment.delete(Appoiment.where(meeting:meeting_subscribe, user: user_subscribe))
    redirect_back fallback_location: root_path
  end

  

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_meeting
      @meeting = Meeting.find(params[:id])
    end

    # Only allow a list of trusted parameters through.
    def meeting_params
      params.require(:meeting).permit(:name, :description)
    end
end
