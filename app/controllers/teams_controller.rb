class TeamsController < ApplicationController
  before_action :set_team, only: %i[ show edit update destroy ]

  # GET /teams or /teams.json
  def index
    @teams = Team.all
  end

  # GET /teams/1 or /teams/1.json
  def show
    # Search functionality 
    if params[:search].nil?
      @components = Component.where(team: @team.id)
    else
      @components = Component.where(team: @team.id).where("lower(name) LIKE ?",  "%#{params[:search].downcase}%")
    end
    
    @product_owner_details = UserDetail.find(@team.product_owner_id)
    @scrum_master_details = UserDetail.find(@team.scrum_master_id)
    
    @tasks_estimations_teams = []

    @team_members = UserDetail.where(team_id: @team.id)
    @team_members_ids = @team_members.select(:id)

    @tasks = Task.where(user_detail: @team_members_ids)

    # Estimation 
    final_feedback=  @tasks.group(:developer_feedback).order(:count).count.reverse_each.first.first
    @the_most_expensive_component = @tasks.group(:component).order('sum_estimation DESC').sum(:estimation).first.first
  
    case final_feedback
    when "underestimated"
      @final_feedback = "Subestimează"
    when "perfectly-estimated"
      @final_feedback = "Estimează corespunzător"
    when "supraestimated"
      @final_feedback = "Supraextimează"
    else
      @final_feedback = "Insuficiente date!"
    end

    #Total story points
    @total_effort = @tasks.sum(:estimation)

    # PIE chart DATA 
    @display_pie = @tasks.group(:developer_feedback).count
    @display_pie["Estimate corespunzător"] = @display_pie.delete "perfectly-estimated"
    @display_pie["Subestimate"] = @display_pie.delete "underestimated"
    @display_pie["Supraestimate"] = @display_pie.delete "supraestimated"

    @display_bar = @tasks.group(:estimation).count
    if @display_bar[1]
      @display_bar["Activități de 1SP"] = @display_bar.delete 1
    end
    if @display_bar[2]
      @display_bar["Activități de 2SP"] = @display_bar.delete 2
    end
    if @display_bar[3]
      @display_bar["Activități de 3SP"] = @display_bar.delete 3
    end
    if @display_bar[5]
      @display_bar["Activități de 5SP"] = @display_bar.delete 5
    end
    if @display_bar[8]
      @display_bar["Activități de 8SP"] = @display_bar.delete 8
    end
    if @display_bar[13]
      @display_bar["Activități de 13SP"] = @display_bar.delete 13
    end
    if @display_bar[21]
      @display_bar["Activități de 13SP"] = @display_bar.delete 21
    end
  end

  # GET /teams/new
  def new
    @team = Team.new
  end

  # GET /teams/1/edit
  def edit
  end

  # POST /teams or /teams.json
  def create
    @team = Team.new(team_params)

    respond_to do |format|
      if @team.save
        format.html { redirect_to @team, notice: "Team was successfully created." }
        format.json { render :show, status: :created, location: @team }
      else
        format.html { render :new, status: :unprocessable_entity }
        format.json { render json: @team.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /teams/1 or /teams/1.json
  def update
    respond_to do |format|
      if @team.update(team_params)
        format.html { redirect_to @team, notice: "Team was successfully updated." }
        format.json { render :show, status: :ok, location: @team }
      else
        format.html { render :edit, status: :unprocessable_entity }
        format.json { render json: @team.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /teams/1 or /teams/1.json
  def destroy
    @team.destroy
    respond_to do |format|
      format.html { redirect_to teams_url, notice: "Team was successfully destroyed." }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_team
      @team = Team.find(params[:id])
    end

    # Only allow a list of trusted parameters through.
    def team_params
      params.require(:team).permit(:product_owner_id, :scrum_master_id, :name)
    end
end
