class ComponentsController < ApplicationController
  before_action :set_component, only: %i[ show edit update destroy ]

  # GET /components or /components.json
  def index
    @components = Component.all
  end

  # GET /components/1 or /components/1.json
  def show
    @task = @component.tasks.build

    if params[:search].nil?
      @component_taks = @component.tasks
    else
      @component_taks = @component.tasks.where("lower(title) LIKE ?",  "%#{params[:search].downcase}%")
    end
   
    @current_user_details = UserDetail.where(user_id: current_user.id).first
 
    if @component_taks.many?
      lead_component_developer = @component.tasks.all.group(:user_detail).order(:count).count.reverse_each.first.first
      @total_SP = @component.tasks.all.sum(:estimation)
      feedback_tasks = @component.tasks.all.group(:developer_feedback).order(:count).count.reverse_each.first.first
      # compose the developer's full name
      @lead_component_developer = lead_component_developer.first_name + " " + lead_component_developer.last_name 
    else
      @lead_component_developer = ""
      @total_SP = 0
    end
    @team = Team.find(@component.team_id)
    
    case feedback_tasks
      when "perfectly-estimated"
        @feedback_tasks = "Estimate corespunzător"
      when "supraestimated"
        @feedback_tasks = "Supraestimate"
      when "underestimated"
        @feedback_tasks = "Subestimate"
      else
        @feedback_tasks = "Nu există activități"
    end 
  end

  # GET /components/new
  def new
    @component = Component.new
    @current_user_details = UserDetail.where(user_id: current_user.id).first
  end

  # GET /components/1/edit
  def edit
    @current_user_details = UserDetail.where(user_id: current_user.id).first
  end

  # POST /components or /components.json
  def create
    @component = Component.new(component_params)
    @team = Team.find(@component.team_id)
    @current_user_details = UserDetail.where(user_id: current_user.id).first
    respond_to do |format|
      if @component.save
        format.html { redirect_to @team, notice: "Component was successfully created." }
        format.json { render :show, status: :created, location: @component }
      else
        format.html { render :new, status: :unprocessable_entity }
        format.json { render json: @component.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /components/1 or /components/1.json
  def update
    @current_user_details = UserDetail.where(user_id: current_user.id).first
    @team = Team.find(@component.team_id)
    respond_to do |format|
      if @component.update(component_params)
        format.html { redirect_to @team, notice: "Component was successfully updated." }
        format.json { render :show, status: :ok, location: @component }
      else
        format.html { render :edit, status: :unprocessable_entity }
        format.json { render json: @component.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /components/1 or /components/1.json
  def destroy
    @current_user_details = UserDetail.where(user_id: current_user.id).first
    @team = Team.find(@component.team_id)
    @component.destroy
    respond_to do |format|
      format.html { redirect_to @team, notice: "Component was successfully destroyed." }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_component
      @component = Component.find(params[:id])
    end

    # Only allow a list of trusted parameters through.
    def component_params
      params.require(:component).permit(:team_id, :name, :description)
    end

end
