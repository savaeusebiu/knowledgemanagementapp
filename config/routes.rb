Rails.application.routes.draw do
  resources :components do
    resources :tasks
  end
  namespace :component do
    resources :tasks
  end
  resources :user_details
  resources :teams
  resources :meetings do
    resources :logs
  end
  namespace :meeting do
    resources :logs
  end
  resources :appoiments
  resources :meetings do
    member do
      get :toggle_subscribe
      get :toggle_unsubscribe
    end
  end
  devise_for :users
  
  resources :users do
    get :search_logs
  end

  root to: "user#index"
end
