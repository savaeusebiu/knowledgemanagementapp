const { environment } = require('@rails/webpacker')

const webpack = require('webpack')
environment.plugin.prepared('Provide',
  new webpack.ProvidePlugin({
    $: 'jquery',
    jQuery: 'jquery',
    Popper: ['pooper.js', 'default']
    
  })
)

module.exports = environment
