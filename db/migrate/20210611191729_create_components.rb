class CreateComponents < ActiveRecord::Migration[6.1]
  def change
    create_table :components do |t|
      t.references :team, null: false, foreign_key: true
      t.string :name
      t.text :description

      t.timestamps
    end
  end
end
