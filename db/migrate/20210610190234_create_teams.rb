class CreateTeams < ActiveRecord::Migration[6.1]
  def change
    create_table :teams do |t|
      t.references :product_owner, null: false
      t.references :scrum_master, null: false
      t.string :name

      t.timestamps
    end
    
    add_foreign_key :teams, :users, column: :product_owner_id
    add_foreign_key :teams, :users, column: :scrum_master_id
  end
end
