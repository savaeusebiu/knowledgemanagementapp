class CreateLogs < ActiveRecord::Migration[6.1]
  def change
    create_table :logs do |t|
      t.string :title
      t.text :content
      t.string :severity
      t.date :date
      t.references :meeting, null: false, foreign_key: true

      t.timestamps
    end
  end
end
