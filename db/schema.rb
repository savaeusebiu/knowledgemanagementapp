# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# This file is the source Rails uses to define your schema when running `bin/rails
# db:schema:load`. When creating a new database, `bin/rails db:schema:load` tends to
# be faster and is potentially less error prone than running all of your
# migrations from scratch. Old migrations may fail to apply correctly if those
# migrations use external dependencies or application code.
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 2021_06_12_095419) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"

  create_table "appoiments", force: :cascade do |t|
    t.bigint "user_id", null: false
    t.bigint "meeting_id", null: false
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.index ["meeting_id"], name: "index_appoiments_on_meeting_id"
    t.index ["user_id"], name: "index_appoiments_on_user_id"
  end

  create_table "components", force: :cascade do |t|
    t.bigint "team_id", null: false
    t.string "name"
    t.text "description"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.index ["team_id"], name: "index_components_on_team_id"
  end

  create_table "logs", force: :cascade do |t|
    t.string "title"
    t.text "content"
    t.string "severity"
    t.date "date"
    t.bigint "meeting_id", null: false
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.index ["meeting_id"], name: "index_logs_on_meeting_id"
  end

  create_table "meetings", force: :cascade do |t|
    t.string "name"
    t.text "description"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
  end

  create_table "pg_search_documents", force: :cascade do |t|
    t.text "content"
    t.string "searchable_type"
    t.bigint "searchable_id"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.index ["searchable_type", "searchable_id"], name: "index_pg_search_documents_on_searchable"
  end

  create_table "tasks", force: :cascade do |t|
    t.string "title"
    t.text "description"
    t.string "first_commit"
    t.string "last_commit"
    t.integer "estimation"
    t.string "developer_feedback"
    t.bigint "user_detail_id", null: false
    t.bigint "component_id", null: false
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.index ["component_id"], name: "index_tasks_on_component_id"
    t.index ["user_detail_id"], name: "index_tasks_on_user_detail_id"
  end

  create_table "teams", force: :cascade do |t|
    t.bigint "product_owner_id", null: false
    t.bigint "scrum_master_id", null: false
    t.string "name"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.index ["product_owner_id"], name: "index_teams_on_product_owner_id"
    t.index ["scrum_master_id"], name: "index_teams_on_scrum_master_id"
  end

  create_table "user_details", force: :cascade do |t|
    t.bigint "user_id", null: false
    t.bigint "team_id", null: false
    t.string "first_name"
    t.string "last_name"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.index ["team_id"], name: "index_user_details_on_team_id"
    t.index ["user_id"], name: "index_user_details_on_user_id"
  end

  create_table "users", force: :cascade do |t|
    t.string "email", default: "", null: false
    t.string "encrypted_password", default: "", null: false
    t.string "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.index ["email"], name: "index_users_on_email", unique: true
    t.index ["reset_password_token"], name: "index_users_on_reset_password_token", unique: true
  end

  add_foreign_key "appoiments", "meetings"
  add_foreign_key "appoiments", "users"
  add_foreign_key "components", "teams"
  add_foreign_key "logs", "meetings"
  add_foreign_key "tasks", "components"
  add_foreign_key "tasks", "user_details"
  add_foreign_key "teams", "users", column: "product_owner_id"
  add_foreign_key "teams", "users", column: "scrum_master_id"
  add_foreign_key "user_details", "teams"
  add_foreign_key "user_details", "users"
end
