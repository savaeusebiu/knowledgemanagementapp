# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the bin/rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: 'Star Wars' }, { name: 'Lord of the Rings' }])
#   Character.create(name: 'Luke', movie: movies.first)

m1 = Meeting.create(name: "Developers Meeting", description: "Developers meeting description")
m2 = Meeting.create(name: "Product Owners Meeting", description: "PO meeting description")
m3 = Meeting.create(name: "Steering Meeting", description: "Steering meeting description")

u1 = User.find(1)
u2 = User.find(2)

po1 = User.find(1)
sm1 = User.find(2)

Appoiment.create(meeting:m2, user: u1)
Appoiment.create(meeting:m3, user: u1)
Appoiment.create(meeting:m1, user: u2)

t1 = Team.create(product_owner:po1, scrum_master:sm1, name:"Mars")
c1 = Component.create(team:t1, name:"Componenta 1", description:"Descrierea Componentei 1")
